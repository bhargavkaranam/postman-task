const axios = require('axios');
const should = require('should');
const io = require('socket.io-client');


const socketURL = 'http://localhost:5000';

const SEND_USER = "Test_User_Send";
const RECEIVE_USER = "Test_User_Receive";


function serverRequest(url, data, callback) {
    
    axios.post(url, data)
    .then((response) => {
        return callback(true, response);
    })
    .catch((err) => {
        console.log(false, err);
    })
}


describe("Chat Server",function(){
    this.timeout(10000);
    
    before(() => {
        serverRequest(socketURL + '/delete', {
            username: SEND_USER
        }, (response) => {
            
        })
    })

    it("Is Socket server up", (done) => {
        serverRequest(socketURL + '/up', {}, (success, response) => {
            if(success) {
                done();
            }
        })
    })

    it("Create new user", (done) => {
        
        let client1 = io.connect(socketURL);
        
        
        
        serverRequest(socketURL + '/join', {
            'username': SEND_USER,
            'local': false
        }, function(success, response){
            
            if(response.data.status) {
                
                client1.on('users', (user) => {
                    
                    if(user === SEND_USER) {
                        done();
                        client1.disconnect();
                    }
                })
            }
        })
        
    })
    
    
    
    
    
    
    
    it("Send and receive a message", (done) => {
        let client1 = io.connect(socketURL);
        let client2 = io.connect(socketURL);
        
        
        let data = {
            from: SEND_USER,
            to: RECEIVE_USER,
            message: "Hello World",
            time: new Date().getTime()
        };
        
        client1.emit('join', SEND_USER);
        client2.emit('join', RECEIVE_USER);
        
        client1.emit('new message', data);
        
        client2.on('message', (d) => {
            
            if(d.from === data.from && d.to === data.to && d.message === data.message) {
                client1.disconnect();
                client2.disconnect();
                done();
            }
        })
        
    })                    
    
});