var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var socket_io    = require( "socket.io" );

// ALL COMMON VARIABLES 
let common = require('./common');

var cors = require('cors');
var index = require('./routes/index');
var users = require('./routes/users');



var app = express();
app.use(cors());
common.io = socket_io();
app.io = common.io;




let currentUsers = []; //FOR ALL THE USERS CURRENTLY ONLINE - HAS USERNAMES
let socketAndUserMap = []; //SOCKET ID AND USERNAME MAPPING

common.io.on( "connection", function( socket )
{
  console.log("User connected");
  common.io.sockets.emit('users', common.currentUsers); //EMIT A SOCKET EVENT WITH THE USERS ARRAY AS SOON AS A NEW CONNECTION IS FORMED
  
  // CLIENT EMITS JOIN EVENT WITH ITS USERNAME AND A CHATROOM IS CREATED FOR PERSONAL CHATS
  socket.on('join', (data) => {
    common.socketAndUserMap[socket.id] = data;
    socket.join(data);
  })
  
  // IS TRIGGERED WHEN A NEW MESSAGE IS SENT
  socket.on('new message', (data) => {
    
    let d = new Date();
    let n = d.toLocaleTimeString(); //SERVER TIMESTAMP
    data.time = n;        
    common.io.sockets.in(data.to).emit('message', data); //EMITTED TO THE PARTICULAR CLIENT
  })
  
  // ON DISCONNECT, USER IS DELETED FROM THE ARRAY
  socket.on('disconnect', () => {
    let username = common.socketAndUserMap[socket.id];    
    common.currentUsers.splice(common.currentUsers.indexOf(username), 1);      
    
  })
});



// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.set('view engine', 'jade');

// WHEN THE USER SENDS A JOIN REQUEST
app.use('/join', users.join);

// FOR TESTING PURPOSES - DELETES THE USER
app.use('/delete', users.del);

app.use('/up', (req, res) => {
  return res.json({status: 'UP'});
})

app.use('/', index);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
