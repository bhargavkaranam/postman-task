var express = require('express');
var router = express.Router();

const common = require('../common');


router.join = (req, res) => {
  if(req.body.username) {
    
    let username = req.body.username;

    // IF LOCAL STORAGE 
    if(req.body.local === true) {
      common.io.sockets.emit('users', username); 
      return res.json({ status: true, message: 'Added.' })
    }
    else if(common.currentUsers.includes(username)) {
      
      return res.json({status: false, message: 'Username already online!', resCode: '101'})
    }

    // PUSHES TO THE CURRENT USERS ARRAY AND EMIT EVENT
    else {      
      common.currentUsers.push(username);  
      common.io.sockets.emit('users', username); 
      return res.json({ status: true, message: 'Added.' })          
    }
    
    
  }
  else {
    return res.json({status: false, message: 'Field required'})
  }
};

router.del = (req, res) => {
  if(req.body.username) {
    common.currentUsers.splice(common.currentUsers.indexOf(req.body.username), 1);
    return res.json({status: true});
  }
  else {
    return res.json({status: false})
  }
};

module.exports = router;
