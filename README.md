# Whatsapp Implementation

# How to run
1. Clone the repo
2. Cd to the directory and type **npm install**
3. Cd to the server folder in the directory and type **npm install**
4. Type **npm start** in both the directories in the terminal
5. Go to http://localhost:3000 on the browser. The server must be running on port 5000
6. For running the tests, in the root directory run **npm run test**

# Modules used
1. ExpressJS - backend
2. SocketIO - sockets
3. DexieJS - IndexedDB wrapper
4. React Search Input - for search filtering
5. Axios - for HTTP requests
6. MochaJS - for tests
7. Create React App - for ReactJS boiler plate

# How it works
1. Whenever a new client logs in, an event is emitted to the server with the username. The server pushes the username to the list of currently online users. A chat room is created for that particular client which it listens to. 
2. The array of users online is sent to the client through a socket. The client then displays in a list
3. Every time a message is sent or received, it is stored in the local DB (IndexedDB). I'm using Dexie, a wrapper for IndexedDB API. 
4. The username and the list of users is stored locally too - LocalStorage. 
5. When a particular contact is selected, the particular message is fetched from the local DB and caches in a variable and this is displayed.
6. Whenever a message is sent and received, a socket event is emitted and the message is displayed in the message area. 
7. The timestamp is set too. 
8. On refreshing the page, username and users are fetched from the local storage. And the messages on clicking a particular user is loaded from the IndexedDB.
9. A toast notification appears whenever a new message is received and the particular contact blinks.