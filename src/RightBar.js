import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import TopNav from './TopNav';
import MessageArea from './MessageArea';
import SendMessage from './SendMessage';

import { view } from 'react-easy-state';
import app from './stores/app';

class RightBar extends Component {
    render() {
        
        let style = {
            display: app.LOADER_STYLE
        }
        return(
            <div className="rightBar">
            <div style={style} class="lds-ring"><div></div><div></div><div></div><div></div></div>
             {app.messageClicked && 
             <div>
                <TopNav image="http://via.placeholder.com/350x150" title={app.currentUser} />   
                <MessageArea />
                <SendMessage />
            </div>
             }
            </div>
        )
    }
}

export default view(RightBar);