import React, { Component } from 'react';
import './App.css';


class TopNav extends Component {
    
    render() {
        return(
            <div className="nav">
                <img className="avatar" src={this.props.image} />
                <h3>{this.props.title}</h3>
            </div>
        )
    }
}

export default TopNav;