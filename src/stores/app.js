import { store } from 'react-easy-state';
import axios from 'axios';
import db from '../db';
import config from '../config';

export default store({
    messageClicked: false,
    currentUser: null,
    enterUsernameModal: true,
    users: [],
    username: '',
    socket: null,
    messages: {},
    SERVER_RESPONSE: '',
    searchTerm: '',

    LOADER_STYLE: 'none',

    blinkingStyle: {},
    
    sendJoinRequest (username, isLocalStorage = false) {
        
        axios.post(config.SERVER_URL + '/join', 
        {
            'username': username,
            'local': isLocalStorage
        })
        .then((response) => {
            
            if(response.data.status) {
                
                this.username = username;
                
                this.enterUsernameModal = false;
                this.socket.emit('join', username);
                localStorage.setItem("username", username);
                
            }
            else {
                if(!this.enterUsernameModal)
                this.enterUsernameModal = true;
                this.SERVER_RESPONSE = response.data.message;
            }
        })
        .catch((err) => {
            console.log("Server down.");
        })
    },
    
    getMessagesFromLocalStorage() {
        let messages;
        if((messages = localStorage.getItem("messages")) !== null)  {                  
            messages = JSON.parse(messages);
            this.messages = messages;
        }
    },
    
    getUsersFromLocalStorage() {
        let users;
        if((users = localStorage.getItem("users")) !== null) {      
            
            users = JSON.parse(users);
            console.log(users);
            users.map((user) => {
                
                if(!this.users.includes(user)) {
                    this.users.push(user);
                }
            })
            
            localStorage.setItem("users", JSON.stringify(this.users));      
        }
    },
    
    insertIntoDatabase(table, data) {

        db[table].put(data)
        .then(() => {
            console.log("Inserted into DB");
        })
        .catch(() => {
            console.log("Error with inserting into the database");
        })
    },

    getCurrentTime() {
      let d = new Date();
      return d.toLocaleTimeString();      
    }
    
})