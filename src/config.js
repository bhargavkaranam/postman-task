const config = {
    SERVER_URL: 'http://localhost:5000',
    DB_NAME: 'whatsapp',
    TABLE_NAME: 'messages'
}

export default config;