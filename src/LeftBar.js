import React, { Component } from 'react';
import { view } from 'react-easy-state';

import './App.css';

import TopNav from './TopNav';
import Search from './Search';
import MessageList from './MessageList';

import app from './stores/app';

class LeftBar extends Component {
    render() {
        return(
            <div className="leftBar">
                <TopNav image="http://via.placeholder.com/350x150" title={app.username}/>
                <Search />
                <MessageList />
            </div>
        )
    }
}

export default view(LeftBar);