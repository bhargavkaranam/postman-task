import Dexie from 'dexie';
const db = new Dexie('whatsapp');
db.version(1).stores({
    messages: '++id, from, to, message, time'
});

export default db;  