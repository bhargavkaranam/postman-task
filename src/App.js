import React, { Component } from 'react';
import io from "socket.io-client";
import { view } from 'react-easy-state';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import './App.css';
import app from './stores/app';
import db from './db';


import UsernameModal from './UsernameModal';
import LeftBar from './LeftBar';
import RightBar from './RightBar';


import config from './config';

class App extends Component {
  constructor() {
    super();
    
    app.socket = io(config.SERVER_URL, {timeout: 2000000});
    
    
    app.socket.on('users', (data) => {
      
      // ARRAY IF ALL THE USERS ARE BEING SENT OR JUST A STRING 
      if(data.constructor === Array) {
        app.users = data;       
        
        // GET USERS FROM LOCAL STORAGE
        app.getUsersFromLocalStorage(); 
      }
      else if(data.constructor === String) {
        if(!app.users.includes(data)) {
          app.users.push(data);
        }                        
        localStorage.setItem("users", JSON.stringify(app.users));                        
        
      }            
      
    })    
    
    app.socket.on('message', (data) => {
      if (!app.messages[data.from]) {
        app.messages[data.from] = [];
      }
      
      toast(`${data.from} says ${data.message}`);
      
      app.messages[data.from].push(data);
      
      if(app.currentUser !== data.from) {
        app.blinkingStyle[data.from] = {
          animation: 'blinker 1s linear infinite'
        }
      }

      
      // INSERT INTO LOCAL DATABASE
      app.insertIntoDatabase(config.TABLE_NAME, data);
      
      
    })                        
    
  }
  
  componentWillMount() {
    let username;
    
    // IF USERNAME EXISTS IN LOCAL STORAGE, HIDE THE MODAL AND SEND A JOIN REQUEST
    if((username = localStorage.getItem("username")) !== null) {
      app.enterUsernameModal = false;            
      app.sendJoinRequest(username, true);            
    }
    
  }
  
  render() {
    return (
      <div className="App">
      
      <UsernameModal />
      <LeftBar />
      <RightBar />
      <ToastContainer />
      </div>
    );
  }
}

export default view(App);
