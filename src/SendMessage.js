import React,  { Component } from 'react';
import './App.css';
import { view } from 'react-easy-state';

import app from './stores/app';
import db from './db';
import config from './config';
class SendMessage extends Component {
    
    constructor() {
        super();
        this.state = {
            message: ''
        }
    }
    
    handleChange = (ev) => {
        this.setState({message: ev.target.value});
    }
    
    isEnter = (ev) => {
        if(ev.keyCode === 13) {
            this.handleClick();
        }
    }

    handleClick = (ev) => {
        
        if(this.state.message !== "") {
            let d = new Date();
            let n = d.toLocaleTimeString();
            
            let data = {
                from: app.username,
                to: app.currentUser,
                message: this.state.message,
                time: n
            };
            
            app.socket.emit('new message', data);
            
            if(!app.messages[app.currentUser]) {
                app.messages[app.currentUser] = [];
            }
            app.messages[app.currentUser].push(data);
            
            app.insertIntoDatabase(config.TABLE_NAME, data);
            
            this.setState({message: ''});
        }
    }
    
    render() {
        return(
            <div className="sendMessage">
            <input onKeyUp={this.isEnter} value={this.state.message} onChange={this.handleChange} className="messageInput" placeholder="Type a message" />
            <button className="sendMessageButton" onClick={this.handleClick}>SEND</button>
            </div>
        )
    }
}

export default view(SendMessage);