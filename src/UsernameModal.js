import React, { Component } from 'react';
import { view } from 'react-easy-state';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import './App.css';
import app from './stores/app';

class UsernameModal extends Component {
    constructor() {
        super();
        this.state = {
            username: ''
        }
        
    }

    componentWillMount() {
        
    }

    handleChange = (ev) => {
        this.setState({username: ev.target.value});
    }
    handleClose = () => {
        console.log("Closed");
    }

    handleSubmit = () => {        
        app.sendJoinRequest(this.state.username);
    }

    render() {
        const actions = [
            
            <FlatButton
            label="Submit"
            primary={true}
            keyboardFocused={true}
            onClick={this.handleSubmit}
            />,
        ];
        
        return (
            <div>
            <MuiThemeProvider>            
            <Dialog
            title="Enter username"
            actions={actions}
            modal={true}
            open={app.enterUsernameModal}            
            >
            <TextField
            hintText="Username"
            onChange={this.handleChange}
            value={this.state.username}
            />
            <div>{app.SERVER_RESPONSE}</div>
            </Dialog>
            </MuiThemeProvider>
            </div>
        );
    }
}

export default view(UsernameModal);