import React, { Component } from 'react';
import './App.css';
import { view } from 'react-easy-state';
import SearchInput, {createFilter} from 'react-search-input'

import app from './stores/app';
import db from './db';

class MessageList extends Component {
    
    handleClick = (from) => (ev) => {

        app.blinkingStyle[from] = {};
        app.LOADER_STYLE = "flex";
        app.messageClicked = true;
        app.currentUser = from;
        if(!app.messages[app.currentUser]) {

            // FETCHES FROM LOCAL DATABASE BASED ON THE USER
            db.messages.where("from").equals(app.currentUser).or("to").equals(app.currentUser).toArray()
            .then(function(msg){
                // SORTS ACCORDING TO THE ID
                msg.sort(function(a,b) {
                    return a.id - b.id;
                })                
                app.LOADER_STYLE = "none";

                // CACHES SO THAT ANOTHER DB QUERY IS NOT MADE
                app.messages[app.currentUser] = msg;
            })
        }
        else {
            app.LOADER_STYLE = "none";
        }
    }
    
    render() {
        
        // FILTER MESSAGES BASED ON THE SEARCH TERM
        const filteredMessages = app.users.filter(createFilter(app.searchTerm, null))

        

        return(
            
            // DISPLAY THE FILTERED MESSAGES
            filteredMessages.map((k) => {
                if(k === app.username) {
                    return false;       //SO THAT THE SAME USER IS NOT DISPLAYED IN THE MESSAGE LIST
                }     
                return <div style={app.blinkingStyle[k]} className="messageBox" onClick={this.handleClick(k)}>
                <div className="userImage">
                <img className="avatar" src="http://via.placeholder.com/350x150" />
                </div>
                <div className="content">
                <div className="user">
                {k}
                </div>
                <div className="msg">
                Available
                </div>
                </div>
                </div>
            })
        )
    }
}

export default view(MessageList);