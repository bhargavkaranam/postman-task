import React, { Component } from 'react';
import { view } from 'react-easy-state';

import './App.css';
import app from './stores/app';

class Search extends Component {
        
    handleChange = (ev) => {
        app.searchTerm = ev.target.value;
        
    }
    
    render() {
        return(
            <div className="searchHolder">
                <input value={app.searchTerm} onChange={this.handleChange} placeholder="Search" type="text" className="searchInput" />
            </div>
        )
    }
}

export default view(Search);