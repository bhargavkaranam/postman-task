import React, { Component } from 'react';

import './App.css';
import { view } from 'react-easy-state';

import app from './stores/app';

class MessageArea extends Component {
    
    render() {
        return <div className="conversation messageArea">
        
        <div className="conversation-container">
        
         {app.messages[app.currentUser] !== undefined && app.messages[app.currentUser].map((k) => {
             return <div>                                
                        <div className={`message ${(k.from === app.username) ? 'sent' : 'received'}`}>
                           {k.message}
                             <span className="metadata">
                                <span className="time">{k.time}</span>
                            </span>
                    </div>
                        
            </div>
        })}
        
        </div>
        </div>
       
    }
}

export default view(MessageArea);